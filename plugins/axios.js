export default function ({ $axios, redirect, store }) {
    $axios.defaults.timeout = 60000;
    $axios.defaults.baseURL = process.env.AXIOS_BASE_URL

    $axios.onError((error) => {
        debugger
        if (error.response?.status === 401) {
            store.commit('common/setSnackbar', {
                color: "error",
                timeout: 5000,
                text: 'Данное действие доступно после регистрации/входа. Зарегистрируйтесь или войдите!',
                show: true
            })
            redirect('/login')
        }
        if (error.response?.status === 400 && error.response?.data?.error === "invalid_grant") {
            store.commit('common/setSnackbar', {
                color: "error",
                timeout: 5000,
                text: 'Неверный логин или пароль',
                show: true
            })
        }
    })
}
