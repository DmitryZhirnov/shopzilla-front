export default interface Discount{
    id:Number
    title: String
    description: String
    images: Array<String>
    image: String
    category: String
    tags: Object
    isFavorite: boolean
}