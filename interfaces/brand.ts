interface Brand {
    id: Number,
    title: string,
    tags: Array<string>
}