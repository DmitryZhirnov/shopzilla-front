export default interface Snackbar {
    color?: string,
    timeout?: number,
    text?: string,
    show: boolean
}