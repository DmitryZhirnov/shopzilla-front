const title = {
  head() {
    return {
      title: this.title,
      meta: [
        {
          hid: "description",
          name: "description",
          content: this.description
        }
      ]
    };
  },
  created () {
    this.$store.commit("common/setPageName", this.title)
  }
}
export default title
