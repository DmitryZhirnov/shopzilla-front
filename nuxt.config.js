import path from "path"
import fs from "fs"
import colors from "vuetify/es5/util/colors"

const env = require("dotenv").config()

export default {
  mode: "universal",
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: "%s - " + process.env.APP_NAME,
    title: process.env.APP_NAME || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "blue" },
  /**
   * Redirect http to https
   */
  //serverMiddleware: ["redirect-ssl"],
  /*
   ** Global CSS
   */
  css: [
    "~/assets/scss/app.scss",
    //{ src: "~/static/fonts/HelveticaNeueCyr/stylesheet.css" },
    { src: "~/static/fonts/SFPro/stylesheet.css" },
    'swiper/dist/css/swiper.css'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "~/plugins/axios",
    { src: "~/plugins/pwa-installer", mode: "client" },
    { src: "~/plugins/nuxt-swiper-plugin.js", ssr: false }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // "@nuxtjs/eslint-module",
    "@nuxtjs/vuetify",
    "@nuxt/typescript-build"
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    // Doc: https://github.com/nuxt-community/dotenv-module
    "@nuxtjs/dotenv",
    "@nuxtjs/proxy",
    "@nuxtjs/auth"
  ],
  proxy: {
    "/api": {
      target: process.env.LARAVEL_ENDPOINT,
      changeOrigin: true,
      secure: false
    },
    "/oauth": {
      target: process.env.LARAVEL_ENDPOINT,
      secure: false,
      changeOrigin: true
    }
  },
  env: env.parsed,
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true,
    headers: {
      common: { Connection: "keep-alive" }
    },
    https: true,
    baseUrl: process.env.AXIOS_BASE_URL
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) { }
  },

  /** Авторизация  */
  auth: {
    redirect: {
      login: '/login',
      logout: '/',
      user: '/user',
      callback:'/login',
      home:'/auth2'
    },
    strategies: {
      password_grant_custom: {
        _scheme: "~/auth/schemes/PassportPasswordScheme.js",
        client_id: process.env.PASSPORT_PASSWORD_GRANT_ID,
        client_secret: process.env.PASSPORT_PASSWORD_GRANT_SECRET,
        endpoints: {
          login: {
            url: "/oauth/token",
            method: "post",
            propertyName: "access_token"
          },
          logout: false,
          user: {
            url: "/api/user"
          }
        }
      },
      google: {
        client_id: process.env.GOOGLE_CLIENT_ID,
        client_secret: process.env.GOOGLE_CLIENT_SECRET
      }
    }
  },
  server: {
    port: process.env.APP_PORT, // default: 3000
    host: "0.0.0.0"
    /* https: {
      key: fs.readFileSync(path.resolve(__dirname, "shoppzilla.ru.key")),
      cert: fs.readFileSync(path.resolve(__dirname, "shoppzilla.ru.crt"))
    } */
  },
  pwa: {
    manifest: {
      "name": "ShopZilla - агрегатор скидок",
      "short_name": "ShopZilla",
      "theme_color": "#1e5786",
      "background_color": "#FF0000",
      "display": "fullscreen",
      "orientation": "portrait",
      "scope": "/",
      "start_url": "/",
      "icons": [
        {
          "src": "/icons/icon-72x72.png",
          "sizes": "72x72",
          "type": "image/png"
        },
        {
          "src": "/icons/icon-96x96.png",
          "sizes": "96x96",
          "type": "image/png"
        },
        {
          "src": "/icons/icon-128x128.png",
          "sizes": "128x128",
          "type": "image/png"
        },
        {
          "src": "/icons/icon-144x144.png",
          "sizes": "144x144",
          "type": "image/png"
        },
        {
          "src": "/icons/icon-152x152.png",
          "sizes": "152x152",
          "type": "image/png"
        },
        {
          "src": "/icons/icon-192x192.png",
          "sizes": "192x192",
          "type": "image/png"
        },
        {
          "src": "/icons/icon-384x384.png",
          "sizes": "384x384",
          "type": "image/png"
        },
        {
          "src": "/icons/icon-512x512.png",
          "sizes": "512x512",
          "type": "image/png"
        }
      ],
      "splash_pages": null
    },
    workbox: {
      cachingExtensions: [
        "workbox-images-cache.js",
        "workbox-discounts-cache.js"
      ]
    }
  }
}
