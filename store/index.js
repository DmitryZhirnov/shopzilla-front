export const state = () => {
  return {
    snackbar: {
      color: 'success',
      timeout: 3000,
      text: 'snackbar',
      show: false
    }
  }
}

export const mutations = {
  clear({ state }) {
    localStorage.clear();
  }
}
