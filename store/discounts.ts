import Discount from "~/interfaces/discount";
import { ActionTree, MutationTree, GetterTree } from 'vuex'

interface SearchProps {
    query: string,
    from?: number,
    size?: number
}

export const state = () => {
    return {
        discounts: Array<Discount>(),
        favorites: Array<Discount>(),
        filtered: Array<Discount>(),
        query: '',
        from: 0,
        size: 0,
        currentDiscount: {}
    };
};

export type DiscountState = ReturnType<typeof state>

const withFavorites = (discounts: Array<Discount>, favorites: Array<Discount>) => {

    return discounts.map((discount, index) => {
        let large = false
        if ((index + 1) % 8 == 0 || ((index - 4) % 8) == 0)
            large = true

        const isFavorite = favorites.findIndex(d => d.id === discount.id) > -1
        return {
            ...discount,
            isFavorite,
            large
        }
    })
}

export const getters: GetterTree<DiscountState, DiscountState> = {
    discountsWithFavorites: (state: DiscountState) => {
        return withFavorites(state.discounts, state.favorites)
    },
    filteredWithFavorites(state: DiscountState) {
        return withFavorites(state.filtered, state.favorites)
    }
}

export const mutations: MutationTree<DiscountState> = {
    load: (state: DiscountState, discounts: Array<Discount>) => {
        state.discounts = discounts
    },
    set_search_options: (state: DiscountState, searchProps: SearchProps) => {
        state.from = searchProps.from || 0
        state.query = searchProps.query
        state.size = searchProps.size || 0

    },
    add(state: DiscountState, discounts: Array<Discount>) {
        state.discounts = state.discounts.concat(discounts)
    },
    load_favorites(state: DiscountState, favorites: Array<Discount>) {
        state.favorites = favorites
    },
    add_favorite(state: DiscountState, discount: Discount) {
        discount = { ...discount, ...{ isFavorite: true } }
        state.favorites.push(discount)
    },
    remove_favorite(state: DiscountState, discount: Discount) {
        discount = { ...discount, ...{ isFavorite: false } }
        state.favorites = state.favorites.filter(f => f.id != discount.id)
    },
    set_current_discount(state: DiscountState, discount: Discount) {
        state.currentDiscount = discount
    },
    set_filtered_discounts(state: DiscountState, discounts: Array<Discount>) {
        state.filtered = discounts
    }
};

export const actions: ActionTree<DiscountState, DiscountState> = {

    async load({ commit }, props: SearchProps) {
        const { items, from, size } = await this.$axios.$get("/api/discounts", {
            params: {
                ...props
            },
        })
        commit("load", items)
        commit("set_search_options", {
            query: props.query,
            from: from + size,
            size
        })
    },

    async paginate({ commit, state }) {
        const { items, from, size } = await this.$axios.$get("/api/discounts", {
            params: {
                query: state.query,
                from: state.from,
                size: state.size
            }
        });
        commit('add', items)
        commit('set_search_options', {
            query: state.query,
            from: from + size,
            size
        })
    },

    async favorites({ commit }) {
        const { data } = await this.$axios.get("/api/discounts/favorites");
        commit('load_favorites', data)
    },

    async favorite({ commit }, discount) {
        await this.$axios.post("/api/discounts/favorite", { id: discount.id });
        commit('add_favorite', discount)
        commit('common/showSnackbar', {
            color: "success",
            timeout: 1200,
            text: `${discount.title} добавлен в Избранное`,
        }, { root: true })
    },

    async unfavorite({ commit }, discount) {
        await this.$axios.post("/api/discounts/unfavorite", { id: discount.id });
        commit('remove_favorite', discount)
        commit('common/showSnackbar', {
            color: "success",
            text: `${discount.title} удален из Избранного`,
            timeout: 1200,
        }, { root: true })
    },

    async load_discount({ commit }, id) {
        const { data } = await this.$axios.get(`/api/discounts/${id}`)
        //console.log(discount);

        commit("set_current_discount", data)
    },

    /**
     * Загрузка скидок на основе фильтра предпочтений пользоватея
     * @param param0 
     */
    async get_filtered_discounts({ commit, state }) {
        const { data } = await this.$axios.get('/api/discounts/filter');
        commit('set_filtered_discounts', data)
    }
};
