import { ActionTree, MutationTree, GetterTree } from 'vuex'
import Category from "~/interfaces/catalog"

export const settings = () => {
    return {
        categories: new Array<Category>(),
        brands: new Array<Brand>(),
        tags: new Array<String>()
    }
}

export const state = () => {
    return {
        settings: {
            categories: new Array<Category>(),
            brands: new Array<Brand>(),
            tags: new Array<String>()
        },
        brands: new Array<Brand>(),
        tags: new Array<string>()
    };
};

export type UserSettingsState = ReturnType<typeof state>
export type UserSettings = ReturnType<typeof settings>

export const getters: GetterTree<UserSettingsState, UserSettingsState> = {

}

export const mutations: MutationTree<UserSettingsState> = {
    /**
     * Добавление категории в избранное
     * @param state 
     * @param category 
     */
    category_favorite(state: UserSettingsState, category: Category) {
        const index = state.settings.categories.findIndex(c => c.id === category.id)
        if (index > -1)
            state.settings.categories.splice(index, 1)
        else
            state.settings.categories.push(category)
    },
    /**
     * Добавление бренда в избранное
     * @param state 
     * @param brand 
     */
    brand_favorite(state: UserSettingsState, brand: Brand) {
        const index = state.settings.brands.findIndex(b => b.id === brand.id)
        if (index === -1)
            state.settings.brands.push(brand)
        state.brands = state.brands.filter(b => b.id !== brand.id)
    },
    /**
     * Удаление бренда из избранного
     * @param state 
     * @param brand 
     */
    brand_unfavorite(state: UserSettingsState, brand: Brand) {
        state.settings.brands = state.settings.brands.filter(b => b.id !== brand.id)
    },
    /**
     * Добавление тега в избранное
     * @param state 
     * @param tag 
     */
    tag_favorite(state: UserSettingsState, tag: string) {
        const index = state.settings.tags.findIndex(t => t === tag)
        if (index === -1)
            state.settings.tags.push(tag)
        state.tags = state.tags.filter(t => t !== tag)
    },
    /**
     * Удаление тега из ибранного
     * @param state 
     * @param tag 
     */
    tag_unfavorite(state: UserSettingsState, tag: string) {
        state.settings.tags = state.settings.tags.filter(t => t !== tag)
    },
    load_brands(state: UserSettingsState, brands: Array<Brand>) {
        state.brands = brands
    },
    load_tags(state: UserSettingsState, tags: Array<string>) {
        state.tags = tags
    },
    /**
     * Установка настроек пользователя
     * @param state 
     * @param settings 
     */
    set_settings(state: UserSettingsState, settings: UserSettings) {
        if(settings !== null)
            state.settings = settings
    }
};

export const actions: ActionTree<UserSettingsState, UserSettingsState> = {
    /**
     * Получение списка брендов для фильтра
     * @param commit
     * @param query - начальные буквы бренда
     */
    async load_brands({ commit }, query: String) {
        const { data } = await this.$axios.get("/api/brands/search", { params: { query } })
        commit("load_brands", data)
    },
    /**
     * Получение списка тегов для фильтра
     * @param commit
     * @param query - начальные буквы тега
     */
    async load_tags({ commit }, query: String) {
        const { data } = await this.$axios.get("/api/tags/search", { params: { query } })
        commit("load_tags", data)
    },
    async save_settings({ state, commit }) {
        try {
            const user = await this.$axios.post("/api/settings/save", { settings: state.settings })
            commit('common/showSnackbar', {
                color: "success",
                timeout: 1200,
                text: `Настройки фильтра успешно сохранены`,
            }, { root: true })
        }
        catch (error) {
            console.error(error)
            commit('common/showSnackbar', {
                color: "error",
                timeout: 1200,
                text: `Не удалось сохранить настройки фильтра, ошибка на сервере!`,
            }, { root: true })
        }
    },
};
