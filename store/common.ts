import { MutationTree, GetterTree } from 'vuex'
import Snackbar from '~/interfaces/snackbar';



export const state = () => {
    return {
        snackbar: {
            color: 'success',
            timeout: 10000,
            text: 'snackbar',
            show: false
        } as Snackbar,
        pageName: "Загрузка..."
    }
};

export type CommonState = ReturnType<typeof state>

export const getters: GetterTree<CommonState, CommonState> = {

}

export const mutations: MutationTree<CommonState> = {
    setSnackbar: (state: CommonState, snackbar: Snackbar) => {
        state.snackbar = { ...state.snackbar, ...snackbar };
    },
    showSnackbar: (state: CommonState, snackbar: Snackbar) => {
        snackbar.show = true
        state.snackbar = { ...state.snackbar, ...snackbar };
    },
    hideSnackbar: (state: CommonState) => {
        state.snackbar = { ...state.snackbar, ...{ show: false } }
    },
    setPageName: (state: CommonState, pageName: string) => {
        state.pageName = pageName
    }
};

