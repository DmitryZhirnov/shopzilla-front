import { ActionTree, MutationTree, GetterTree } from 'vuex'
import Catalog from '~/interfaces/catalog';

export const state = () => {
    return {
        parentId: 0,
        categories: Array<Catalog>()
    };
};

export type CatalogState = ReturnType<typeof state>

export const getters: GetterTree<CatalogState, CatalogState> = {

}

export const mutations: MutationTree<CatalogState> = {
    load: (state: CatalogState, categories: Array<Catalog>) => {
        state.categories = categories
    },
};

export const actions: ActionTree<CatalogState, CatalogState> = {
    async load({ commit }) {
        const { data } = await this.$axios.get('/api/categories')

        commit('load', data)
    }
};
